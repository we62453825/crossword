import random

n = 15
Across_Marked = [0] * (n**2)
Down_Marked = [0] * (n**2)
Number_Marked = [0] * (n**2)

HASH = '#'
BLANK = '_'
rows = 15
cols = 15
num_blocked = 30

def crossword(rows, cols, num_blocked) -> list[list]:
    grid = [[BLANK for _ in range(cols)] for _ in range(rows)]
    blocked_boxes = random.sample(range(rows * cols), num_blocked)
    for box in blocked_boxes:
        grid[box // cols][box % cols] = HASH
    return grid

def Get_Box_Num(x: int, y: int, n: int) -> int:
    return (x - 1) * n + (y - 1)

def Get_Coordinate(num: int, n: int) -> list:
    return [num // n + 1, num % n + 1]

def Across_Word(num: int, grid: list, n: int) -> bool:
    x, y = Get_Coordinate(num, n)
    if y <= n and grid[x - 1][y - 1] == '_':
        start_y = y - 1
        # Find the end of the dashes in this row
        while start_y < n and grid[x - 1][start_y] == '_':
            start_y += 1
        # Mark all boxes that are part of this horizontal word
        for j in range(y - 1, start_y):
            Across_Marked[Get_Box_Num(x, j + 1, n)] = 1
        return start_y - (y - 1) >= 2  # Ensure word length is at least 2
    return False

def Down_Word(num: int, grid: list, n: int) -> bool:
    x, y = Get_Coordinate(num, n)
    if x <= n and grid[x - 1][y - 1] == '_':
        start_x = x - 1
        # Find the end of the dashes in this column
        while start_x < n and grid[start_x][y - 1] == '_':
            start_x += 1
        # Mark all boxes that are part of this vertical word
        for i in range(x - 1, start_x):
            Down_Marked[Get_Box_Num(i + 1, y, n)] = 1
        return start_x - (x - 1) >= 2  # Ensure word length is at least 2
    return False

def Assign_Number(grid: list, n: int) -> list:
    result = []
    Number = 0
    for i in range(n):
        for j in range(n):
            if grid[i][j] == '_' and (Across_Marked[Get_Box_Num(i + 1, j + 1, n)] == 0 or Down_Marked[Get_Box_Num(i + 1, j + 1, n)] == 0):
                Box_Num = Get_Box_Num(i + 1, j + 1, n)
                if Across_Marked[Box_Num] == 0 and Across_Word(Box_Num, grid, n):
                    Number += 1
                    Number_Marked[Box_Num] = Number
                    result.append((i + 1, j + 1, Number))
                elif Down_Marked[Box_Num] == 0 and Down_Word(Box_Num, grid, n):
                    Number += 1
                    Number_Marked[Box_Num] = Number
                    result.append((i + 1, j + 1, Number))
    return result


grid = crossword(rows, cols, num_blocked)
print("\nGenerated Grid:")
for row in grid:
    print(' '.join(row))


result = Assign_Number(grid, n)
print("\nAssigned Numbers:")
for x, y, number in result:
    print(f"({x}, {y}, {number})")

