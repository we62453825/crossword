CrossWord Problem

----
----
____
INPUT FORMAT:
____ 
     A randomly generated grid of '#' and '_'
    '#' for a black space, '_' for a white space, we take an NxN grid, of which N is assumed to be 15.
     ex: #_________
         ###_______
         ______####
         ##________
         _________#
         #####_____
         ________##
         ###_______
         __________
         #####_____


____
APPROACH
____

Overall:
We solve the question by iterating over each box of the the grid. We have variable Across and Down for each box that are initially marked false, which is marked True once its the start or a part of an Across or Down word respectively. For each White Box, if Across is false, we check if any string of horizontal whitespaces follow the grid, if yes it gets a number (we use variable NUMBER to keep a track of it).Same with Down, we check if any string of vertical whitespaces follow the box, if yes it gets a number iff it did not already get a number from across. 


_____
OUTPUT FORMAT:
_____
The randomly generated grid,
The coordinates of boxes to which a number is assigned and the number assigned to that box.
ex: (1,1,1)
    (1,2,2)
    and so on...
The indicing is considered to start from one.
